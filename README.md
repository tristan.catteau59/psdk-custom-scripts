# Pour installer le répo : 

- Installer Git : https://git-scm.com/downloads
- Aller dans le dossier scripts à la racine de votre projet
- Click-droit, ``Git Bash here`` (redémarrer votre ordinateur si vous n'avez pas l'option)
- Copier/Coller dans la fenêtre de commande : ``git clone git@gitlab.com:Zodiarche/psdk-custom-scripts.git``
- Modifier le nom du fichier pour : ``00000 psdk-custom-scripts``
