module Battle
  class Logic
    Hooks.remove(StatChangeHandler, :stat_increase_prevention, 'PSDK stat incr: Effects')

    StatChangeHandler.register_stat_increase_prevention_hook('PSDK stat incr: Effects') do |handler, stat, target, launcher, skill|
      next handler.logic.each_effects(target, launcher) do |effect|
        next effect.on_stat_increase_prevention(handler, stat, target, launcher, skill)
      end
    end
  end
end
