module Battle
  class Logic
    # Handler responsive of answering properly ability changes requests
    class AbilityChangeHandler < ChangeHandlerBase
      include Hooks

      # A pokémon cannot lose these talents
      Battle::Logic::AbilityChangeHandler.send(:remove_const, :CANT_OVERWRITE_ABILITIES)
      CANT_OVERWRITE_ABILITIES = %i[as_one battle_bond comatose commander disguise gulp_missile hadron_engine hunger_switch
                                    ice_face imposter multitype orichalcum_pulse power_construct protosynthesis quark_drive 
                                    rks_system schooling shields_down stance_change wonder_guard zen_mode zero_to_hero
      ]

      # A pokémon cannot receive these talents
      Battle::Logic::AbilityChangeHandler.send(:remove_const, :RECEIVER_CANT_COPY_ABILITIES)
      CANT_RECEIVE_ABILITIES = CANT_OVERWRITE_ABILITIES + %i[flower_gift forecast illusion neutralizing_gas power_of_alchemy receiver trace]

      # A move fails if the target has
      Battle::Logic::AbilityChangeHandler.send(:remove_const, :SKILL_BLOCKING_ABILITIES)
      SKILL_BLOCKING_ABILITIES = {
        entrainment: CANT_RECEIVE_ABILITIES + %i[truant],
        role_play: CANT_RECEIVE_ABILITIES + %i[truant],
        skill_swap: CANT_RECEIVE_ABILITIES,
        simple_beam: %i[truant],
        worry_seed: %i[truant]
      }

      # A talent is not activated if the target has
      Battle::Logic::AbilityChangeHandler.send(:remove_const, :ABILITY_BLOCKING_ABILITIES)
      ABILITY_BLOCKING_ABILITIES = {
        mummy: %i[mummy],
        lingering_aroma: %i[lingering_aroma],
        wandering_spirit: CANT_RECEIVE_ABILITIES + %i[wandering_spirit],
        trace: CANT_RECEIVE_ABILITIES,
        receiver: CANT_RECEIVE_ABILITIES,
        power_of_alchemy: CANT_RECEIVE_ABILITIES
      }

      # Function that change the ability of a Pokemon
      # @param target [PFM::PokemonBattler]
      # @param ability_symbol [Symbol, :none] Symbol ID of the Ability
      # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
      # @param skill [Battle::Move, nil] Potential move used
      def change_ability(target, ability_symbol, launcher = nil, skill = nil)
        target.ability = (ability_symbol == :none ? 0 : data_ability(ability_symbol).id) || 0
        exec_hooks(AbilityChangeHandler, :post_ability_change, binding)
      end
    end

    Hooks.remove(AbilityChangeHandler, :ability_change_prevention, 'PSDK Ability Prev: Cannot OW Target Ability')
    Hooks.remove(AbilityChangeHandler, :ability_change_prevention, 'PSDK Ability Prev: Cannot OW Target Ability With Skill')
    Hooks.remove(AbilityChangeHandler, :ability_change_prevention, 'PSDK Ability Prev: Cannot OW Target Ability With Ability')
    Hooks.remove(AbilityChangeHandler, :ability_change_prevention, 'PSDK Ability Prev: Cannot OW User Ability With Skill')

    # Cannot overwrite specific abilities
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Cannot OW Target Ability') do |handler, target, _, _, _|
      next unless AbilityChangeHandler::CANT_OVERWRITE_ABILITIES.include?(target.battle_ability_db_symbol)

      next handler.prevent_change # silent
    end

    # Cannot overwrite/receive specific abilities with Entrainment
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Entrainment') do |handler, target, _, launcher, skill|
      next unless skill&.db_symbol == :entrainment
      next unless AbilityChangeHandler::SKILL_BLOCKING_ABILITIES[skill.db_symbol]&.include?(launcher.battle_ability_db_symbol)

      next handler.prevent_change # silent
    end

    # Cannot overwrite/receive specific abilities with Role Play
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Role Play') do |handler, target, _, launcher, skill|
      next unless skill&.db_symbol == :role_play
      next unless AbilityChangeHandler::CANT_OVERWRITE_ABILITIES.include?(launcher.battle_ability_db_symbol) ||
        AbilityChangeHandler::SKILL_BLOCKING_ABILITIES[skill.db_symbol]&.include?(target.battle_ability_db_symbol)

      next handler.prevent_change # silent
    end

    # Cannot receive specific abilities with Skill Swap
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Skill Swap') do |handler, target, _, launcher, skill|
      next unless skill&.db_symbol == :skill_swap
      next unless AbilityChangeHandler::SKILL_BLOCKING_ABILITIES[skill.db_symbol]&.include?(target.battle_ability_db_symbol) ||
       AbilityChangeHandler::SKILL_BLOCKING_ABILITIES[skill.db_symbol]&.include?(launcher.battle_ability_db_symbol)

      next handler.prevent_change # silent
    end

    # Cannot receive specific abilities with Skill Swap
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Skill Swap') do |handler, target, _, launcher, skill|
      next unless skill && %i[simple_beam worry_seed].include?(skill.db_symbol)
      next unless AbilityChangeHandler::SKILL_BLOCKING_ABILITIES[skill.db_symbol]&.include?(target.battle_ability_db_symbol)

      next handler.prevent_change # silent
    end

    # Cannot overwrite specific abilities with Mummy / Lingering Aroma / Wandering Spirit
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Mummy/Lingering Aroma/Wandering Spirit') do |handler, target, ability_symbol, _, skill|
      next if skill
      next unless %i[mummy lingering_aroma wandering_spirit].include?(ability_symbol)
      next unless AbilityChangeHandler::ABILITY_BLOCKING_ABILITIES[ability_symbol]&.include?(target.battle_ability_db_symbol)
      
      next handler.prevent_change # silent
    end

    # Cannot receive specific abilities with Receiver / Power of Alchemy / Trace
    AbilityChangeHandler.register_ability_prevention_hook('PSDK Ability Prev: Receiver/PowerOfAlchemy/Trace') do |handler, target, ability_symbol, _, skill|
      next if skill
      next unless %i[receiver power_of_alchemy trace].include?(target.battle_ability_db_symbol)
      next unless AbilityChangeHandler::ABILITY_BLOCKING_ABILITIES[target.battle_ability_db_symbol]&.include?(ability_symbol)
      
      next handler.prevent_change # silent
    end
  end
end
