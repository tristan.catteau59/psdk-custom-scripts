module Battle
  module Effects
    # Class describing all the effect ("abstract") and helping the handler to manage effect
    class EffectBase
      # Give the atk modifier over given to the Pokemon with this effect
      # @return [Float, Integer] multiplier
      def atk_modifier
        return 1
      end

      # Give the dfe modifier over given to the Pokemon with this effect
      # @return [Float, Integer] multiplier
      def dfe_modifier
        return 1
      end

      # Give the ats modifier over given to the Pokemon with this effect
      # @return [Float, Integer] multiplier
      def ats_modifier
        return 1
      end

      # Give the dfs modifier over given to the Pokemon with this effect
      # @return [Float, Integer] multiplier
      def dfs_modifier
        return 1
      end

      private

      # Function that disable all the hooks (putting aside on_delete)
      def disable_hooks
        class << self
          alias atk_modifier base_power_multiplier
          alias dfe_modifier base_power_multiplier
          alias ats_modifier base_power_multiplier
          alias dfs_modifier base_power_multiplier
        end
      end
    end
  end
end
