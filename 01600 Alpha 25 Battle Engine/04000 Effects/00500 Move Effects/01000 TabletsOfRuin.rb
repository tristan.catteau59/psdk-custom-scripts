module Battle
  module Effects
    class TabletsOfRuin < EffectBase
      # Give the atk modifier over given to the Pokemon with this effect
      # @return [Float, Integer] multiplier
      def atk_modifier
        return 0.75
      end

      # Function giving the name of the effect
      # @return [Symbol]
      def name
        return :tablets_of_ruin
      end
    end
  end
end