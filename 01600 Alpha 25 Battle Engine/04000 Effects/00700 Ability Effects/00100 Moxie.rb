module Battle
  module Effects
    class Ability
      class Moxie < Ability
        # Create a new Moxie effect
        # @param logic [Battle::Logic]
        # @param target [PFM::PokemonBattler]
        # @param db_symbol [Symbol] db_symbol of the ability
        def initialize(logic, target, db_symbol)
          super
          @affect_allies = true
        end

        # Function called after damages were applied and when target died (post_damage_death)
        # @param handler [Battle::Logic::DamageHandler]
        # @param hp [Integer] number of hp (damage) dealt
        # @param target [PFM::PokemonBattler]
        # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
        # @param skill [Battle::Move, nil] Potential move used
        def on_post_damage_death(handler, hp, target, launcher, skill)
          return if launcher != @target || launcher == target
          return if skill&.be_method == :s_fell_stinger
          return unless launcher.alive? && handler.logic.stat_change_handler.stat_increasable?(:atk, launcher)

          handler.scene.visual.show_ability(launcher)
          handler.scene.visual.wait_for_animation
          handler.logic.stat_change_handler.stat_change(:atk, 1, launcher)
        end
      end
      register(:moxie, Moxie)
      register(:chilling_neigh, Moxie)
    end
  end
end
