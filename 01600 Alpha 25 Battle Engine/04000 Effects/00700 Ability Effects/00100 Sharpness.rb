module Battle
  module Effects
    class Ability
      class Sharpness < Ability
        # Give the move base power mutiplier
        # @param user [PFM::PokemonBattler] user of the move
        # @param target [PFM::PokemonBattler] target of the move
        # @param move [Battle::Move] move
        # @return [Float, Integer] multiplier
        def base_power_multiplier(user, target, move)
          return super if user =! @target
          return super unless move.slice_attack?

          return 1.5
        end
      end
    register(:sharpness, Sharpness)
    end
  end
end