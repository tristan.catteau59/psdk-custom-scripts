module Battle
  module Effects
    class Ability
      class WanderingSpirit < Ability
        # Function called after damages were applied (post_damage, when target is still alive)
        # @param handler [Battle::Logic::DamageHandler]
        # @param hp [Integer] number of hp (damage) dealt
        # @param target [PFM::PokemonBattler]
        # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
        # @param skill [Battle::Move, nil] Potential move used
        def on_post_damage(handler, hp, target, launcher, skill)
          return if target != @target || launcher == @target
          return unless skill&.direct? && launcher&.hp > 0 && !launcher.has_ability?(:long_reach)
          return unless handler.logic.ability_change_handler.can_change_ability?(launcher, db_symbol)

          handler.scene.visual.show_ability(target)
          handler.scene.visual.wait_for_animation
          handler.scene.display_message_and_wait(parse_text_with_pokemon(19, 508, target))

          launcher_ability = ability_symbol(launcher)
          change_ability(launcher, ability_symbol(target))
          change_ability(target, launcher_ability)
        end

        # Function called after damages were applied and when target died (post_damage_death)
        # @param handler [Battle::Logic::DamageHandler]
        # @param hp [Integer] number of hp (damage) dealt
        # @param target [PFM::PokemonBattler]
        # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
        # @param skill [Battle::Move, nil] Potential move used
        def on_post_damage_death(handler, hp, target, launcher, skill)
          on_post_damage(handler, hp, target, launcher, skill)
        end

        # Function that changes the ability of the pokemon given
        # @param pokemon [PFM::PokemonBattler]
        # @param ability_symbol [Symbol, :none] Symbol of the Ability
        def change_ability(pokemon, ability_symbol)
          handler.scene.visual.show_ability(pokemon)
          handler.scene.visual.wait_for_animation
          handler.logic.ability_change_handler.change_ability(pokemon, ability_symbol)
          handler.scene.visual.show_ability(pokemon)
          handler.scene.visual.wait_for_animation
          handler.scene.display_message_and_wait(parse_text_with_pokemon(19, 405, pokemon, PFM::Text::ABILITY[1] => pokemon.ability_name))
        end

        # Function that returns the ability of the pokemon given
        # @param pokemon [PFM::PokemonBattler]
        # @return [Symbol]
        def ability_symbol(pokemon)
          return pokemon.battle_ability_db_symbol
        end
      end
      register(:wandering_spirit, WanderingSpirit)
    end
  end
end