module Battle
  module Effects
    class Ability
      class WindRider < Ability
        # Create a new Wind Rider effect
        # @param logic [Battle::Logic]
        # @param target [PFM::PokemonBattler]
        # @param db_symbol [Symbol] db_symbol of the ability
        def initialize(logic, target, db_symbol)
          super
          @activated = false
        end

        # Function called when a Pokemon has actually switched with another one
        # @param handler [Battle::Logic::SwitchHandler]
        # @param who [PFM::PokemonBattler] Pokemon that is switched out
        # @param with [PFM::PokemonBattler] Pokemon that is switched in
        def on_switch_event(handler, who, with)
          return if who == @target || with != @target
          return (@activated = true) if handler.logic.bank_effects[@target.bank].has?(:tailwind)

          nil
        end

        # Function called at the end of an action
        # @param logic [Battle::Logic] logic of the battle
        # @param scene [Battle::Scene] battle scene
        # @param battlers [Array<PFM::PokemonBattler>] all alive battlers
        def on_post_action_event(logic, scene, battlers)
          return unless logic.bank_effects[@target.bank].has?(:tailwind)
          return if @activated

          @activated = true
          logic.scene.visual.show_ability(@target)
          logic.stat_change_handler.stat_change_with_process(:atk, 1, @target)
        end

        # Function called when a damage_prevention is checked
        # @param handler [Battle::Logic::DamageHandler]
        # @param hp [Integer] number of hp (damage) dealt
        # @param target [PFM::PokemonBattler]
        # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
        # @param skill [Battle::Move, nil] Potential move used
        # @return [:prevent, Integer, nil] :prevent if the damage cannot be applied, Integer if the hp variable should be updated
        def on_damage_prevention(handler, hp, target, launcher, skill)
          return if target != @target
          return unless skill.wind_attack?

          return handler.prevent_change do
            handler.scene.visual.show_ability(target)
            handler.logic.stat_change_handler.stat_change_with_process(:atk, 1, target, launcher, skill)
          end
        end
      end
      register(:wind_rider, WindRider)
    end
  end
end