module Battle
  module Effects
    class Ability
      class QuarkDrive < Ability
        # Create a new FlowerGift effect
        # @param logic [Battle::Logic]
        # @param target [PFM::PokemonBattler]
        # @param db_symbol [Symbol] db_symbol of the ability
        def initialize(logic, target, db_symbol)
          super
          @highest_stat = nil
        end

        # Function called when a Pokemon has actually switched with another one
        # @param handler [Battle::Logic::SwitchHandler]
        # @param who [PFM::PokemonBattler] Pokemon that is switched out
        # @param with [PFM::PokemonBattler] Pokemon that is switched in
        def on_switch_event(handler, who, with)
          return if with != @target
          return unless handler.logic.terrain_effects.has?(:electric_terrain) && with.hold_item?(:booster_energy)

          handler.scene.visual.show_ability(with)
          handler.scene.visual.wait_for_animation
          @highest_stat = boost_highest_stat
        end

        # Function called after the terrain was changed
        # @param handler [Battle::Logic::FTerrainChangeHandler]
        # @param fterrain_type [Symbol] :none, :electric_terrain, :grassy_terrain, :misty_terrain, :psychic_terrain
        # @param last_fterrain [Symbol] :none, :electric_terrain, :grassy_terrain, :misty_terrain, :psychic_terrain
        def on_post_fterrain_change(handler, fterrain_type, last_fterrain)
          return if @highest_stat || fterrain_type != :electric_terrain

          handler.scene.visual.show_ability(with)
          handler.scene.visual.wait_for_animation
          @highest_stat = boost_highest_stat
        end

        # Function called to increase the pokémon's highest stat
        # @param handler [Battle::Logic::FTerrainChangeHandler]
        def boost_highest_stat
          stats = { atk: @target.atk, dfe: @target.dfe, ats: @target.ats, dfs: @target.dfs, spd: @target.spd }

          highest_value = stats.values.max
          highest_stat_key = stats.key(highest_value)
          return highest_stat_key.to_sym
        end

        # Give the atk modifier over given to the Pokemon with this effect
        # @return [Float, Integer] multiplier
        def atk_modifier
          return super unless @highest_stat == :atk

          return 1.3
        end

        # Give the dfe modifier over given to the Pokemon with this effect
        # @return [Float, Integer] multiplier
        def dfe_modifier
          return super unless @highest_stat == :dfe

          return 1.3
        end

        # Give the ats modifier over given to the Pokemon with this effect
        # @return [Float, Integer] multiplier
        def ats_modifier
          return super unless @highest_stat == :ats

          return 1.3
        end

        # Give the dfs modifier over given to the Pokemon with this effect
        # @return [Float, Integer] multiplier
        def dfs_modifier
          return super unless @highest_stat == :dfs

          return 1.3
        end

        # Give the speed modifier over given to the Pokemon with this effect
        # @return [Float, Integer] multiplier
        def spd_modifier
          return super unless @highest_stat == :spd

          return 1.5
        end
      end
      register(:quark_drive, QuarkDrive)
    end
  end
end