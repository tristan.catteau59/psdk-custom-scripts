module Battle
  module Effects
    class Ability
      class TabletsOfRuin < Ability
        # Function called when a Pokemon has actually switched with another one
        # @param handler [Battle::Logic::SwitchHandler]
        # @param who [PFM::PokemonBattler] Pokemon that is switched out
        # @param with [PFM::PokemonBattler] Pokemon that is switched in
        def on_switch_event(handler, who, with)
          return if with != @target

          foes = handler.logic.all_alive_battlers.reject { |pokemon| pokemon == with }
          return if foes.empty?

          foes.each do |foe|
            next if foe.has_ability?(:tablets_of_ruin) || foe.effects.has?(:tablets_of_ruin)

            foe.effects.add(Effects::TabletsOfRuin.new(handler.logic))
          end

          handler.scene.visual.show_ability(with)
          handler.scene.visual.wait_for_animation
          #handler.scene.display_message_and_wait(parse_text_with_pokemon(x, y, with)) TODO: Créer le CSV et rajouter le texte correspondant
        end
      end
      register(:tablets_of_ruin, TabletsOfRuin)
    end
  end
end
