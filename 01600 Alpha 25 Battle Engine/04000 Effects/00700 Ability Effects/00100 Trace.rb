module Battle
  module Effects
    class Ability
      class Trace < Ability
        # Function called when a Pokemon has actually switched with another one
        # @param handler [Battle::Logic::SwitchHandler]
        # @param who [PFM::PokemonBattler] Pokemon that is switched out
        # @param with [PFM::PokemonBattler] Pokemon that is switched in
        def on_switch_event(handler, who, with)
          return if with != @target

          foes = handler.logic.foes_of(with).select { |foe| handler.logic.ability_change_handler.can_change_ability?(with, foe.battle_ability_db_symbol) }
          return if foes.empty?

          target = foes.sample(random: handler.logic.generic_rng)
          handler.scene.visual.show_ability(with)
          handler.scene.visual.wait_for_animation

          handler.logic.ability_change_handler.change_ability(with, target.battle_ability_db_symbol)
          handler.scene.visual.show_ability(with)
          handler.scene.visual.wait_for_animation
          handler.scene.display_message_and_wait(parse_text_with_pokemon(19, 381, target, PFM::Text::ABILITY[1] => with.ability_name))
          with.ability_effect.on_switch_event(handler, who, with)
        end
      end
      register(:trace, Trace)
    end
  end
end
