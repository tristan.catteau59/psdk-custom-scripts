module Battle
  module Effects
    class Ability
      class ArmorTail < Ability
        # Function called when we try to check if the user cannot use a move
        # @param user [PFM::PokemonBattler]
        # @param move [Battle::Move]
        # @return [Proc, nil]
        def on_move_disabled_check(user, move)
          return if user == @target || user.bank == @target
          return unless user&.can_be_lowered_or_canceled?
          return unless move&.priority(user) > 7 && move&.one_target?

          return proc {
            move.scene.visual.show_ability(@target)
            move.scene.visual.wait_for_animation
            move.scene.display_message_and_wait(parse_text_with_pokemon(19, 911, user, PFM::Text::MOVE[1] => move.name))
          }

        end
      end
      register(:armor_tail, ArmorTail)
    end
  end
end