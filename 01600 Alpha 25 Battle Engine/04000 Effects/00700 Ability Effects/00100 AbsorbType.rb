module Battle
    module Effects
      class Ability
        class VoltAbsorb < Ability
          # Function called when a damage_prevention is checked
          # @param handler [Battle::Logic::DamageHandler]
          # @param hp [Integer] number of hp (damage) dealt
          # @param target [PFM::PokemonBattler]
          # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
          # @param skill [Battle::Move, nil] Potential move used
          # @return [:prevent, Integer, nil] :prevent if the damage cannot be applied, Integer if the hp variable should be updated
          def on_damage_prevention(handler, hp, target, launcher, skill)
            return if target != @target || @target.effects.has?(:heal_block)
            return unless skill && check_move_type?(skill)
            return unless launcher&.can_be_lowered_or_canceled?
  
            return handler.prevent_change do
              handler.scene.visual.show_ability(target)
              handler.logic.damage_handler.heal(target, target.max_hp / factor)
            end
          end

          # Function that checks the type of the move
          # @param skill [Battle::Move, nil] Potential move used
          # @return [Boolean]
          def check_move_type?(skill)
            return skill.type_electric?
          end

          # Returns the factor used for healing
          # @return [Integer]
          def factor
            4
          end
        end

        class WaterAbsorb < Ability
          # Function that checks the type of the move
          # @param skill [Battle::Move, nil] Potential move used
          # @return [Boolean]
          def check_move_type?(skill)
            return skill.type_water?
          end
        end

        class EarthEater < Ability
          # Function that checks the type of the move
          # @param skill [Battle::Move, nil] Potential move used
          # @return [Boolean]
          def check_move_type?(skill)
            return skill.type_ground?
          end
        end

        register(:volt_absorb, VoltAbsorb)
        register(:water_absorb, WaterAbsorb)
        register(:earth_eater, EarthEater)
      end
    end
  end
  