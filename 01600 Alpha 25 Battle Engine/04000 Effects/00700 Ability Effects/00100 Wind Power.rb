module Battle
  module Effects
    class Ability
      class WindPower < Ability
        # Create a new Wind Rider effect
        # @param logic [Battle::Logic]
        # @param target [PFM::PokemonBattler]
        # @param db_symbol [Symbol] db_symbol of the ability
        def initialize(logic, target, db_symbol)
          super
          @activated = false
          @move = nil
        end

        # Function called when a Pokemon has actually switched with another one
        # @param handler [Battle::Logic::SwitchHandler]
        # @param who [PFM::PokemonBattler] Pokemon that is switched out
        # @param with [PFM::PokemonBattler] Pokemon that is switched in
        def on_switch_event(handler, who, with)
          return if who == @target || with != @target
          return (@activated = true) if handler.logic.bank_effects[@target.bank].has?(:tailwind)

          nil
        end

        # Function called at the end of an action
        # @param logic [Battle::Logic] logic of the battle
        # @param scene [Battle::Scene] battle scene
        # @param battlers [Array<PFM::PokemonBattler>] all alive battlers
        def on_post_action_event(logic, scene, battlers)
          return unless logic.bank_effects[@target.bank].has?(:tailwind)
          return if @activated

          @activated = true
          @move = data_move(:tailwind).name
          logic.scene.visual.show_ability(@target)
        end

        # Function called when a damage_prevention is checked
        # @param handler [Battle::Logic::DamageHandler]
        # @param hp [Integer] number of hp (damage) dealt
        # @param target [PFM::PokemonBattler]
        # @param launcher [PFM::PokemonBattler, nil] Potential launcher of a move
        # @param skill [Battle::Move, nil] Potential move used
        # @return [:prevent, Integer, nil] :prevent if the damage cannot be applied, Integer if the hp variable should be updated
        def on_damage_prevention(handler, hp, target, launcher, skill)
          return if target != @target
          return unless skill.wind_attack?

          return handler.prevent_change do
            handler.scene.visual.show_ability(target)
            @boosted = skill.name
          end
        end

        # Give the move base power mutiplier
        # @param user [PFM::PokemonBattler] user of the move
        # @param target [PFM::PokemonBattler] target of the move
        # @param move [Battle::Move] move
        # @return [Float, Integer] multiplier
        def base_power_multiplier(user, target, move)
          return super unless @move || move.type_electric?

          #TODO: Add the message : "Being hit by <@move> charged <name (move.name here)> with power!"
          @move = nil
          return 1.5
        end
      end
      register(:wind_power, WindPower)
    end
  end
end