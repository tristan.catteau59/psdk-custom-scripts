module Battle
  class Move
    class CustomEntrainment < Move
      # Function that tests if the user is able to use the move
      # @param user [PFM::PokemonBattler] user of the move
      # @param targets [Array<PFM::PokemonBattler>] expected targets
      # @note Thing that prevents the move from being used should be defined by :move_prevention_user Hook
      # @return [Boolean] if the procedure can continue
      def move_usable_by_user(user, targets)
        return false unless super

        if targets.none? { |target| @logic.ability_change_handler.can_change_ability?(target, :none, user, self) && target.battle_ability_db_symbol != ability_symbol(user) }
          return show_usage_failure(user) && false
        end

        return true
      end

      # Function that deals the effect to the pokemon
      # @param user [PFM::PokemonBattler] user of the move
      # @param actual_targets [Array<PFM::PokemonBattler>] targets that will be affected by the move
      def deal_effect(user, actual_targets)
        actual_targets.each do |target|
          next unless @logic.ability_change_handler.can_change_ability?(target, :none, user, self)
          
          handler.scene.display_message_and_wait(parse_text_with_pokemon(19, 508, user))
          change_ability(target, ability_symbol(user))
        end
      end

      # Function that changes the ability of the pokemon given
      # @param pokemon [PFM::PokemonBattler]
      # @param ability_symbol [Symbol, :none] Symbol of the Ability
      def change_ability(pokemon, ability_symbol)
        @scene.visual.show_ability(pokemon)
        @scene.visual.wait_for_animation
        @logic.ability_change_handler.change_ability(pokemon, ability_symbol, pokemon, self)
        @scene.visual.show_ability(pokemon)
        @scene.visual.wait_for_animation
        @scene.display_message_and_wait(parse_text_with_pokemon(19, 405, pokemon, PFM::Text::ABILITY[1] => pokemon.ability_name))
      end

      # Function that returns the ability of the pokemon given
      # @param pokemon [PFM::PokemonBattler]
      # @return [Symbol]
      def ability_symbol(pokemon)
        return pokemon.battle_ability_db_symbol
      end
    end

    class CustomRolePlay < Entrainment
      # Function that deals the effect to the pokemon
      # @param user [PFM::PokemonBattler] user of the move
      # @param actual_targets [Array<PFM::PokemonBattler>] targets that will be affected by the move
      def deal_effect(user, actual_targets)
        actual_targets.each do |target|
          next unless @logic.ability_change_handler.can_change_ability?(target, :none, user, self)

          change_ability(user, ability_symbol(target))
        end
      end
    end

    class CustomSkillSwap < Entrainment
      # Function that deals the effect to the pokemon
      # @param user [PFM::PokemonBattler] user of the move
      # @param actual_targets [Array<PFM::PokemonBattler>] targets that will be affected by the move
      def deal_effect(user, actual_targets)
        actual_targets.each do |target|
          next unless @logic.ability_change_handler.can_change_ability?(target, :none, user, self)

          user_ability = ability_symbol(user)
          change_ability(user, ability_symbol(target))
          user.ability_effect&.on_switch_event(@logic.switch_handler, nil, user)

          change_ability(target, user_ability)
          target.ability_effect&.on_switch_event(@logic.switch_handler, nil, target)
        end
      end
    end

    class SimpleBeam < Entrainment
      # Function that returns the ability of the pokemon given
      # @param pokemon [PFM::PokemonBattler]
      # @return [Symbol]
      def ability_symbol(pokemon)
        return :simple
      end
    end

    class WorrySeed < Entrainment
      # Function that returns the ability of the pokemon given
      # @param pokemon [PFM::PokemonBattler]
      # @return [Symbol]
      def ability_symbol(pokemon)
        return :insomnia
      end
    end

    Move.register(:s_entrainment, CustomEntrainment)
    Move.register(:s_role_play, CustomRolePlay)
    Move.register(:s_skill_swap, CustomSkillSwap)
    Move.register(:s_simple_beam, SimpleBeam)
    Move.register(:s_worry_seed, WorrySeed)
  end
end