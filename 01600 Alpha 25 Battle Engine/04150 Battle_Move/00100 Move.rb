module Battle
  class Move
    WIND_MOVES = %i[air_cutter bleakwind_storm blizzard fairy_wind gust heat_wave hurricane icy_wind petal_blizzard 
                    sandsear_storm sandstorm springtide_storm tailwind twister whirlwind wildbolt_storm
    ]

    SLICING_MOVES = %i[aerial_ace air_cutter air_slash aqua_cutter behemoth_blade bitter_blade ceaseless_edge
                      cross_poison cut fury_cutter kowtow_cleave leaf_blade night_slash population_bomb psyblade
                      psycho_cut razor_leaf razor_shell sacred_sword slash solar_blade stone_axe x_scissor
    ]

    # Is the skill a wind attack ?
    # @return [Boolean]
    def wind_attack?
      return WIND_MOVES.include?(db_symbol)
    end

    # Is the skill a slice attack ?
    # @return [Boolean]
    def slice_attack?
      return SLICING_MOVES.include?(db_symbol)
    end
  end
end