module PFM
  class PokemonBattler
    # Return the current atk
    # @return [Integer]
    def atk
      raw_atk = (atk_basis * atk_modifier).floor

      return @scene.logic.each_effects(self).reduce(raw_atk) do |product, e|
        (product * e.atk_modifier).floor
      end

    # Return the current dfe
    # @return [Integer]
    def dfe
      raw_dfe = (dfe_basis * dfe_modifier).floor
      return @scene.logic.each_effects(self).reduce(raw_dfe) do |product, e|
        (product * e.dfe_modifier).floor
      end
    end

    # Return the current ats
    # @return [Integer]
    def ats
      raw_ats = (ats_basis * ats_modifier).floor
      return @scene.logic.each_effects(self).reduce(raw_ats) do |product, e|
        (product * e.ats_modifier).floor
      end
    end

    # Return the current dfs
    # @return [Integer]
    def dfs
      raw_dfs = (dfs_basis * dfs_modifier).floor
      return @scene.logic.each_effects(self).reduce(raw_dfs) do |product, e|
        (product * e.dfs_modifier).floor
      end    
    end
    end
  end
end
